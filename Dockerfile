FROM alpine
ARG host
ENV host ${host}
WORKDIR /opt/icons
CMD ["/bin/sh" , "-c", "wget --no-check-certificate --output-document=/opt/icons/$host-favicon.ico $host/favicon.ico"]
